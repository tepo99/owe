import {useEffect, useState} from "react";
import UserList from "../users/UserList";
import {Button, CircularProgress, Paper, TextField} from "@material-ui/core";
import axios from "axios";
import {useSnackbar} from "notistack";
import {Add} from "@material-ui/icons";
import {useStyles} from "../../styles/styles";

/**
 * Component handling participants rendering and adding for specified event
 */
const ParticipantsPicker = (props) => {
    let {enqueueSnackbar} = useSnackbar();
    let [participants, setParticipants] = useState([]);
    let [loading, setLoading] = useState(true);
    let [newParticipant, setNewParticipant] = useState("");
    const classes = useStyles();

    useEffect(() => {
        if (props.participants.length > 0) {
            axios.post("/api/users/info", props.participants)
                .then((result) => {
                    setParticipants(result.data);
                }).catch((error) => {
                enqueueSnackbar("Failed to load participant" + error.message&&": " + error.message, {variant: "error"});
            }).finally(() => setLoading(false));
        } else {
            setLoading(false);
        }
    }, [props.participants, enqueueSnackbar]);

    let handleParticipantAdd = (event) => {
        event.preventDefault();
        setLoading(true);
        axios.post('/api/events/add-participant/' + props.eventId, {search: newParticipant})
            .then((result) => {
                setNewParticipant("");
                enqueueSnackbar("Participant added successfully", {variant: "success"});
                setParticipants(result.data);
            }).catch((error) => {
            enqueueSnackbar("Failed to add participant" + (error.response.data.message&&": " + error.response.data.message), {variant: "error"});
        }).finally(() => setLoading(false));
    };

    let handleParticipantRemove = (id) => {
        setLoading(true);
        axios.post('/api/events/remove-participant/' + props.eventId, {_id: id} )
            .then(() => {
                setParticipants(participants.filter(participant => participant._id !== id));
                enqueueSnackbar("Participant removed", {variant: "success"});
                setLoading(false);
            }).catch((error) => {
            enqueueSnackbar("Failed to remove participant" + (error.response.data.message&&": " + error.response.data.message), {variant: "error"});
            setLoading(false);
        });
    };

    if (loading) {
        return <CircularProgress/>;
    }

    return (<>
        <Paper className={classes.wrapper}>
            <UserList users={participants} onParticipantDelete={handleParticipantRemove}/>
            <TextField type="text" placeholder="Add participant" value={newParticipant} onChange={(event) => {
                setNewParticipant(event.target.value);
            }}/>
            <Button variant="contained" color="primary" disableElevation size="small" type="submit" onClick={handleParticipantAdd}><Add/></Button>
        </Paper>
    </>);
};

export default ParticipantsPicker;