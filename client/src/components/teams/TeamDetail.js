import React, {useEffect, useState} from "react";
import {useHistory, useParams} from "react-router";
import {Backdrop, Card, CircularProgress, IconButton} from "@material-ui/core";
import MemberPicker from "./MemberPicker";
import Navigation from "../Navigation";
import axios from "axios";
import {useStyles} from "../../styles/styles";
import {useSnackbar} from "notistack";
import {Delete} from "@material-ui/icons";

/**
 * Displays details of a team and allows for adding members
 */
const TeamDetail = () => {
    let params = useParams();
    let history = useHistory();
    let {enqueueSnackbar} = useSnackbar();
    let [loading, setLoading] = useState(true);
    let [team, setTeam] = useState({});
    let classes = useStyles();

    useEffect(() => {
        axios.get("/api/teams/get/" + params.id)
            .then((result) => {
                setTeam(result.data);
            }).catch((error) => {
            enqueueSnackbar("Failed to load team" + (error.response.data.message&&": " + error.response.data.message), {variant: "error"});
        }).finally(() => setLoading(false));
    }, [params.id, enqueueSnackbar]);

    let handleDelete = () => {
        setLoading(true);
        axios.delete("/api/teams/delete/"+params.id).then(() => {
               enqueueSnackbar("Team deleted", {variant: "success"});
               return history.goBack();
        }).catch((error) => {
            enqueueSnackbar("Failed to delete team" + (error.response.data.message&&": " + error.response.data.message), {variant: "error"});
            return setLoading(false);
        });
    };

    return (<Card className={classes.column}>
        <Navigation>
            <div className={classes.autoCenter}/>
            <IconButton onClick={handleDelete}><Delete/></IconButton>
        </Navigation>
        <Backdrop open={loading} className={classes.backdrop}>
            <CircularProgress/>
        </Backdrop>
        <h1>{team.name}</h1>
        <section>{team.description}</section>
        {!loading && <MemberPicker members={team.members} teamId={team._id}/>}
    </Card>);
};

export default TeamDetail;