import {useState} from "react";
import Navigation from "../Navigation";
import MyTeams from "./MyTeams";
import MemberTeams from "./MemberTeams";
import {Button, Card, IconButton} from "@material-ui/core";
import {Link} from "react-router-dom";
import {Add} from "@material-ui/icons";
import {useStyles} from "../../styles/styles";

/**
 * Helper component for switching between owned and member teams
 */
const Teams = () => {
    let [myTeams, setMyTeams] = useState(true);
    let classes = useStyles();

    return (<Card className={classes.column}>
        <Navigation>
            <Button onClick={() => {
                setMyTeams(!myTeams);
            }}
                    className={classes.autoCenter}>{myTeams ? "Show teams I'm a member of" : "Show teams I have created"}</Button>
            <Link to="/team/add"><IconButton><Add/></IconButton></Link>
        </Navigation>
        {myTeams ? <MyTeams/> : <MemberTeams/>}
    </Card>);

};

export default Teams;