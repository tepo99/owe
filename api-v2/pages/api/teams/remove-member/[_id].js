import {useUser} from "../../../../auth/hooks";
import TeamModel from "../../../../model/team";
import connectDB from "../../../../middleware/mongodb";

const handler = async (req, res) => {
    if(req.method !== 'POST') {
        return res.status(405).send({message: "Method not allowed."});
    }
    let {_id} = req.query;
    let user = useUser(req);
    if (!user) {
        return res.status(401).send();
    }
    let team = await TeamModel.findOne({_id: _id});
    if(!team) {
        return res.status(404).send();
    }
    // noinspection EqualityComparisonWithCoercionJS (disabled in favor of forced typecasting)
    team.members = team.members.filter(member => member != req.body._id);
    team.save();
    return res.status(200).send();
};

export default connectDB(handler);