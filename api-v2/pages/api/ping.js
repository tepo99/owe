import connectDB from "../../middleware/mongodb";
import {useUser} from "../../auth/hooks";

const handler = async (req, res) => {
    if(req.method !== 'GET') {
        return res.status(405).send({message: "Method not allowed."});
    }
    let user = await useUser(req);
    if(!user) {
        return res.status(401).send("User not found");
    }
    return res.status(200).send();
};

export default connectDB(handler);