import React from "react"
import { reducer, initialState } from "./reducer"

export const LoginContext = React.createContext({
    state: initialState,
    dispatch: () => null
});

export const LoginProvider = ({ children }) => {
    const [state, dispatch] = React.useReducer(reducer, initialState);

    return (
        <LoginContext.Provider value={[ state, dispatch ]}>
            { children }
        </LoginContext.Provider>
    );
};