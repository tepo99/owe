import React, { useEffect, useState} from "react";
import {Backdrop, Card, CircularProgress, IconButton} from "@material-ui/core";
import axios from "axios";
import {useStyles} from "../../styles/styles";
import Navigation from "../Navigation";
import Day from "../calendar/Day";
import {Link} from "react-router-dom";
import {Add} from "@material-ui/icons";
import {useSnackbar} from "notistack";

/**
 * Komponenta starajici se o nacteni a roztrideni nadchazejicich udalosti
 */
const Agenda = () => {
    let {enqueueSnackbar} = useSnackbar();
    let [loading, setloading] = useState(true);
    let [events, setEvents] = useState([]);
    let classes = useStyles();


    useEffect(() => {        
        axios.get("/api/events/agenda")
            .then((response) => {
            setEvents(response.data);
        }).catch((error) => {
            enqueueSnackbar("Failed to load agenda" + (error.response.data.message&&": " + error.response.data.message), {variant: "error"});
        }).finally(() => setloading(false));

    }, [enqueueSnackbar]);


    const getAgenda = () => {
        let agendaGroups = [];
        let groupItems = [];
        let sameDayDatePart = undefined;

        let i = 0;
        events.forEach(event => {
            let eventStartDate = event.startDate;
            let currentEventDatePart = eventStartDate.substring(0, eventStartDate.indexOf("T"));

            if(sameDayDatePart === undefined || sameDayDatePart === currentEventDatePart) {
                if(groupItems[i] === undefined) {
                    groupItems[i] = [];
                }
                groupItems[i].push(event);
            }
            else{
                i++;
                groupItems[i] = [];
                groupItems[i].push(event);
            }
            sameDayDatePart = currentEventDatePart;
        });
        groupItems.forEach(dayEvents => {
            let date = new Date(dayEvents[0].startDate);
            agendaGroups.push(<Card className={classes.item}><Day year={date.getFullYear()} month={date.getMonth()} day={date.getDate()} events={dayEvents} isAgenda /></Card>);
        });

        return agendaGroups;
    };
    
    return<Card className={classes.column}>
        <Navigation>
            <div className={classes.autoCenter}/>
            <Link to="/event/add"><IconButton><Add/></IconButton></Link>
        </Navigation>
        <Backdrop open={loading} className={classes.backdrop}>
            <CircularProgress/>
        </Backdrop>
        {getAgenda()}
    </Card>;
};


export default Agenda;