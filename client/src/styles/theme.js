import {createMuiTheme} from "@material-ui/core";
import {amber, teal} from "@material-ui/core/colors";

export const brightTheme = createMuiTheme({
    palette: {
        primary: teal,
        secondary: amber,
    },
});