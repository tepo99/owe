import {Backdrop, Button, Card, CircularProgress, TextField} from "@material-ui/core";
import React, {useState} from "react";
import {Link} from "react-router-dom";
import axios from "axios";
import {useSnackbar} from "notistack";
import {useStyles} from "../styles/styles";

/**
 * Component that handles signing up new users
 */
const SignupForm = () => {
    let {enqueueSnackbar} = useSnackbar();
    let [username, setUsername] = useState("");
    let [usernameError, setUsernameError] = useState("");
    let [email, setEmail] = useState("");
    let [emailError, setEmailError] = useState("");
    let [password, setPassword] = useState("");
    let [passwordError, setPasswordError] = useState("");
    let [passwordCheck, setPasswordCheck] = useState("");
    let [passwordCheckError, setPasswordCheckError] = useState("");
    let [loading, setLoading] = useState(false);
    let classes = useStyles();

    const handleUsername = (event) => {
        let error = "";
        if (event.target.value === "") {
            error = "Field required";
        }
        setUsernameError(error);
        setUsername(event.target.value);
    };

    const handleEmail = (event) => {
        let error = "";
        if (!/.+@.+\..+/.test(event.target.value)) {
            error = "Not a valid email";
        }
        if (event.target.value === "") {
            error = "Field required";
        }
        setEmailError(error);
        setEmail(event.target.value);
    };

    const handlePassword = (event) => {
        let error = "";
        if (event.target.value.length < 8) {
            error = "Must be at least eight characters";
        }
        if (event.target.value === "") {
            error = "Field required";
        }
        setPasswordError(error);
        setPassword(event.target.value);
    };

    const handlePasswordCheck = (event) => {
        let error = "";
        if (event.target.value !== password) {
            error = "Passwords do not match";
        }
        if (event.target.value === "") {
            error = "Field required";
        }
        setPasswordCheckError(error);
        setPasswordCheck(event.target.value);
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        setLoading(true);
        if (usernameError !== "" || emailError !== "" || passwordError !== "" || passwordCheckError !== "") {
            enqueueSnackbar("Could not sign up. Please check the form and try again.", {variant: "error"});
            setLoading(false);
            return;
        }
        let user = {
            username: username,
            email: email,
            password: password
        };
        axios.post("/api/users/signup", user)
            .then(() => {
                enqueueSnackbar("Sign up successful! You can now log in.", {variant: "success"});
            }).catch(() => {
            enqueueSnackbar("Could not sign up, please try again.", {variant: "error"});
        }).finally(() => setLoading(false));
    };

    return (
        <Card className={classes.loginCard}>
            <Backdrop open={loading} className={classes.backdrop}>
                <CircularProgress/>
            </Backdrop>
            <h1>Sign up</h1>
            <form onSubmit={handleSubmit}>
                <div>
                    <TextField required error={usernameError} helperText={usernameError} disabled={loading}
                               id="username" label="User name" type="text" value={username} onChange={handleUsername}/>
                </div>
                <div>
                    <TextField required error={emailError} helperText={emailError} disabled={loading} id="email"
                               label="E-mail" type="email" value={email} onChange={handleEmail}/>
                </div>
                <div>
                    <TextField required error={passwordError} helperText={passwordError} disabled={loading}
                               id="password" label="Password" type="password" value={password}
                               onChange={handlePassword}/>
                </div>
                <div>
                    <TextField required error={passwordCheckError} helperText={passwordCheckError} disabled={loading}
                               id="password-check" label="Repeat password" type="password" value={passwordCheck}
                               onChange={handlePasswordCheck}/>
                </div>
                <Button disabled={loading} className={classes.loginButton} color="primary" variant="contained"
                        type="submit">Sign up</Button>
            </form>
            <p>Already have an account? <Link to="/login">Log in</Link></p>
        </Card>
    );
};

export default SignupForm;