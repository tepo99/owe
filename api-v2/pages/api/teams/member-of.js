import connectDB from "../../../middleware/mongodb";
import {useUser} from "../../../auth/hooks";
import TeamModel from "../../../model/team";

const handler = async (req, res) => {
    if(req.method !== 'GET') {
        return res.status(405).send({message: "Method not allowed."});
    }
    let user = await useUser(req);
    if (!user) {
        return res.status(401).send("Not logged in");
    }
    TeamModel.find({members: user._id}, (error, result) => {
        if (error) {
            return res.status(500).send(error);
        }
        res.status(200).send(result);
    });
};

export default connectDB(handler);

