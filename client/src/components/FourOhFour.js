import {Link} from "react-router-dom";

/**
 * 404 page
 */
const FourOhFour = () => {
    return <div>
        <h1>404 - not found</h1>
        <p>Looks like this page does not exist. Try going back to the <Link to="/">Calendar</Link>.</p>
    </div>;
};

export default FourOhFour;