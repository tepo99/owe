import {Container, Divider, IconButton, Paper} from "@material-ui/core";
import {Link} from "react-router-dom";
import {Edit, Person} from "@material-ui/icons";
import {useStyles} from "../../styles/styles";

/**
 * Component for displaying an item in a list of teams
 */
const TeamItem = props => {
    let classes = useStyles();

    return (<Paper className={classes.outer}>
        <Container className={classes.grid}>
            <Container className={classes.edit}>
                {props.editable ? <><Link to={`/team/edit/${props.team._id}`}><IconButton
                    size="small"><Edit/></IconButton></Link><Divider orientation="vertical"/></>:""}
            </Container>
            <Container>
                <Link to={`/team/detail/${props.team._id}`}>{props.team.name}</Link>
                {props.team.description&&<Container>{props.team.description}</Container>}
            </Container>
            <Container className={classes.members}>
                <Divider orientation="vertical"/>
                <Person/> {props.team.members.length}
            </Container>
        </Container>
    </Paper>);
};

export default TeamItem;