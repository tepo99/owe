import { MAX_AGE, setTokenCookie, getTokenCookie } from './auth-cookies'
import * as jwt from "jsonwebtoken";

const TOKEN_SECRET = 'TOP_SECRET';

export async function setLoginSession(res, session) {
    let createdAt = Date.now();
    // Create a session object with a max age that we can validate later
    let body = { ...session, createdAt, maxAge: MAX_AGE };
    let token = await jwt.sign(body, TOKEN_SECRET, {expiresIn: "24h"});

    setTokenCookie(res, token);
}

export async function getLoginSession(req) {
    const token = getTokenCookie(req);

    if (!token) return;

    const session = await jwt.verify(token, TOKEN_SECRET);
    const expiresAt = session.createdAt + session.maxAge * 1000;

    // Validate the expiration date of the session
    if (Date.now() > expiresAt) {
        throw new Error('Session expired');
    }

    return session;
}