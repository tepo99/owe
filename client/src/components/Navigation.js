import {Link} from "react-router-dom";
import {useHistory} from "react-router";
import {useContext} from "react";
import {LoginContext} from "../contexts/LoginContext";
import {AppBar, IconButton} from "@material-ui/core";
import {ExitToApp, Home, People, Schedule} from "@material-ui/icons";
import {useStyles} from "../styles/styles";
import axios from "axios";

/**
 * Header of the application. Allows to add additional items to the header via it's children
 */
const Navigation = (props) => {

    let history = useHistory();
    let dispatch = useContext(LoginContext)[1];
    let classes = useStyles();

    const logout = async () => {
        axios.post('/api/users/logout').then(()=> {
            dispatch({type: "logout"});
            history.push("/login");
        });
    };

    return <AppBar position="sticky" className={classes.appBar}>
        <Link to="/"><IconButton><Home/></IconButton></Link>
        {props.children}
        <Link to="/agenda"><IconButton><Schedule/></IconButton></Link>
        <Link to="/teams"><IconButton><People/></IconButton></Link>
        <Link to="/login" onClick={logout}><IconButton><ExitToApp/></IconButton></Link>
    </AppBar>;
};

export default Navigation;