import connectDB from "../../../../middleware/mongodb";
import TeamModel from "../../../../model/team";
import UserModel from "../../../../model/user";
import {useUser} from "../../../../auth/hooks";

const handler = async (req, res) => {
    if(req.method !== 'POST') {
        return res.status(405).send({message: "Method not allowed."});
    }
    let {_id} = req.query;
    let user = await useUser(req);
    if (!user) {
        return res.status(401).send("Not logged in");
    }
    TeamModel.findOne({_id: _id}, (error, team) => {
        if (error) {
            return res.status(400).send(error);
        }
        if (!team) {
            return res.status(404).send("Team not found");
        }
        UserModel.findOne({$or: [{username: req.body.search}, {email: req.body.search}]}, "_id username email", (error, user) => {
            if (error) throw error;
            if (team.members.includes(user._id)) {
                return res.status(400).send("User is already a member");
            }
            team.members.push(user._id);
            team.save();
            res.status(200).send(user);
        });

    });
};

export default connectDB(handler);