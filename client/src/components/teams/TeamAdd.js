import Navigation from "../Navigation";
import TeamForm from "./TeamForm";
import {useState} from "react";
import {useHistory} from "react-router";
import axios from "axios";
import {useSnackbar} from "notistack";
import {Container} from "@material-ui/core";
import {useStyles} from "../../styles/styles";

/**
 * Handler component for adding a new team via TeamForm component
 */
const TeamAdd = () => {
    let {enqueueSnackbar} = useSnackbar();
    let history = useHistory();
    let [team, setTeam] = useState({});
    let [loading, setLoading] = useState(false);
    let classes = useStyles();

    let handleEdit = (edit) => {
        setTeam({...team, ...edit});
    };

    let handleSubmit = () => {
        setLoading(true);
        axios.post("/api/teams/add", team)
            .then((result) => {
                enqueueSnackbar("Team added successfully", {variant: "success"});
                history.push("/team/edit/" + result.data._id);
            }).catch((error) => {
            enqueueSnackbar("Failed to add team" + (error.response.data.message&&": " + error.response.data.message), {variant: "error"});
            setLoading(false);
        });
    };

    return (<Container className={classes.column}>
        <Navigation>
            <div className={classes.autoCenter}/>
        </Navigation>
        <TeamForm team={team} loadig={loading} editHandler={handleEdit} submitHandler={handleSubmit}/>
    </Container>);
};

export default TeamAdd;