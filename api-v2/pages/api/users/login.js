import passport from "passport";
import nextConnect from "next-connect";
import {localStrategy} from "../../../auth/password-local";
import {setLoginSession} from "../../../auth/auth";

const authenticate = (method, req, res) => {
    return new Promise((resolve, reject) => {
        passport.authenticate(method, {session: false}, (error, token) => {
            if (error) {
                reject(error);
            } else {
                resolve(token);
            }
        })(req, res);
    });
};

passport.use(localStrategy);

export default nextConnect()
    .use(passport.initialize())
    .post(async (req, res) => {
        try {
            const user = await authenticate('local', req, res);
            // session is the payload to save in the token, it may contain basic info about the user
            const session = {username: user.username, _id: user._id };
            await setLoginSession(res, session);

            res.status(200).send({ done: true });
        } catch (error) {
            res.status(401).send(error.message);
        }
    })