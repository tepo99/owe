import {Chip, Container} from "@material-ui/core";
import {Person} from "@material-ui/icons";
import {useStyles} from "../../styles/styles";

/**
 * Displays a list of users based on provided props
 */
const UserList = (props) => {

    let classes = useStyles();

    return (
        <Container className={classes.wrapper}>
            {props.users.map(user => <Chip color="primary" key={user._id} label={user.username} icon={<Person/>}
                                           className={classes.item} onDelete={() => props.onParticipantDelete(user._id)}/>)}
        </Container>);
};

export default UserList;