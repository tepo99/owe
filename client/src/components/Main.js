import {Container} from "@material-ui/core";
import {Redirect, Route, Switch} from "react-router-dom";
import ProtectedRoute from "./ProtectedRoute";
import EventForm from "./events/EventForm";
import Calendar from "./calendar/Calendar";
import Agenda from "./agenda/Agenda";
import LoginForm from "./LoginForm";
import FourOhFour from "./FourOhFour";
import React from "react";
import SignupForm from "./SignupForm";
import Teams from "./teams/Teams";
import TeamDetail from "./teams/TeamDetail";
import TeamEdit from "./teams/TeamEdit";
import TeamAdd from "./teams/TeamAdd";
import {useStyles} from "../styles/styles";

const Main = () => {
    let currentDate = new Date();
    let currentDateUri = `/${currentDate.getFullYear()}/${currentDate.getMonth()}`;
    let classes = useStyles();

    return <Container className={classes.root}>
        <Switch>
            <ProtectedRoute path="/team/add" component={TeamAdd}/>
            <ProtectedRoute path="/team/edit/:id" component={TeamEdit}/>
            <ProtectedRoute path="/team/detail/:id" component={TeamDetail}/>
            <ProtectedRoute path="/teams" component={Teams}/>
            <ProtectedRoute path="/event/add/:date" component={EventForm}/>
            <ProtectedRoute path="/event/add" component={EventForm}/>
            <ProtectedRoute path="/event/:id" component={EventForm}/>
            <ProtectedRoute path="/agenda" component={Agenda}/>
            <ProtectedRoute path="/:year/:month" component={Calendar}/>
            <Redirect exact from="/" to={currentDateUri}/>
            <Route exact path="/login" component={LoginForm}/>
            <Route exact path="/signup" component={SignupForm}/>
            <Route component={FourOhFour}/>
        </Switch>
    </Container>;
};

export default Main;