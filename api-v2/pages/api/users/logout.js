import connectDB from "../../../middleware/mongodb";
import {removeTokenCookie} from "../../../auth/auth-cookies";

const handler = (req, res) => {
    if(req.method !== 'POST') {
        return res.status(405).send({message: "Method not allowed."});
    }
    removeTokenCookie(res);
    return res.status(200).send("Logout successful");
};

export default connectDB(handler);