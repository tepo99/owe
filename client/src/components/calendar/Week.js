import Day from "./Day";
import React from "react";
import {Container, Divider} from "@material-ui/core";
import {useStyles} from "../../styles/styles";

/**
 * Helper component for rendering a single week of a month
 */
const Week = function (props) {
    let classes = useStyles();

    let startOfMonth = new Date(props.year, props.month, 1);
    let startOfWeek = props.week * 7 - startOfMonth.getDay() + (startOfMonth.getDay() === 0 ? -6 : 1) + 1;
    let children = [];
    for (let i = 0; i < 7; i++) {
        children.push(<Container className={classes.dayWrapper}
                                 key={`${props.year}-${props.month}-day${startOfWeek + i}`}>
            <Day
                year={props.year} month={props.month} day={startOfWeek + i}
                events={props.events}
            />
            {i !== 6 && <Divider orientation="vertical"/>}
        </Container>);
    }
    return <>
        <div className={classes.weekGrid}
             key={`wrapper${props.year}-${props.month}-${props.week}`}
        >
            {children}
        </div>
    </>;
};

export default Week