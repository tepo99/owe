import mongoose, {Schema} from 'mongoose';
import UserModel from "./user";

/**
 * Defines a Mongoose schema for a team entry composed out of users
 * @type {Schema}
 */
const TeamSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: String,
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        ref: UserModel,
        required: true
    },
    members: [{type: mongoose.Schema.Types.ObjectId, ref: UserModel}]
});

TeamSchema.path("_id");

const TeamModel = mongoose.models.Team || mongoose.model("Team", TeamSchema);

export default TeamModel;