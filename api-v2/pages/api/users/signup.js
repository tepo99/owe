import passport from "passport";
import connectDB from "../../../middleware/mongodb";

const handler = () => {
    if(req.method !== 'POST') {
        return res.status(405).send({message: "Method not allowed."});
    }
    passport.authenticate('signup', {session: false},
        (req, res) => {
            res.json({
                message: 'Signup successful',
                user: {
                    _id: req.user._id,
                    username: req.user.username,
                },
            });
        });
};

export default connectDB(handler);