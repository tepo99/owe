import {Container, IconButton} from "@material-ui/core";
import CalendarEvent from "./CalendarEvent";
import {useHistory} from "react-router";
import {format} from "date-fns";
import {Add} from "@material-ui/icons";
import {useStyles} from "../../styles/styles";

/**
 * Component used for rendering a day of the calendar
 * Finds it's own events in the provided in props.events
 */
const Day = function (props) {
    let history = useHistory();
    let date = new Date(props.year, props.month, props.day, 0, 0, 0, 0);
    let currentDate = new Date();
    let children = [];
    let classes = useStyles();

    //vyfiltruj eventy pro sebe
    if (props.events.length > 0) {
        props.events.forEach((event) => {
            let diff = new Date(event.startDate) - date; //rozdil mezi dvema daty: 0 - 24h v ms
            if (diff > 0 && diff < 24 * 3600 * 1000/*24 hodin*/) {
                children.push(<Container className={classes.singleEvent} key={event._id}>
                        <CalendarEvent event={event} isAgenda={props.isAgenda}/>
                    </Container>
                );
            }
        });
    }

    let style;
    if(parseInt(props.month) !== date.getMonth()) {
        style = classes.foreignDay;
    } else if(parseInt(props.year) === currentDate.getFullYear() && parseInt(props.month) === currentDate.getMonth() && parseInt(props.day) === currentDate.getDate()) {
        style = classes.today;
    } else {
        style = classes.day;
    }

    return <Container className={style}>
        <Container className={props.isAgenda&&classes.agendaDay}>{props.isAgenda?format(date, "d. MMM. yyyy"):date.getDate()}.</Container>
        <Container className={classes.eventsWrapper}>
            {children}
            {(parseInt(props.month) === date.getMonth() &&
            props.isAgenda) ||
            <IconButton className={classes.addEventButton} onClick={() => {
                history.push("/event/add/" + format(date, "yyyy-MM-dd"));
            }}><Add/></IconButton>}
        </Container>
    </Container>;
};

export default Day;