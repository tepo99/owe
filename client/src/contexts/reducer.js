export const reducer = (state, action) => {
    switch (action.type) {
        case "login":
            return {
                loggedIn: true,
                username: action.username,
                email: action.email
            };
        case "logout":
            return initialState;
        default:
            return state;
    }
};

export const initialState = {
    loggedIn: false,
    username: false,
    email: false,
};