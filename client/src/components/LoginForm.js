import {Backdrop, Button, Card, CircularProgress, TextField} from "@material-ui/core";
import React, {useContext, useEffect, useRef, useState} from "react";
import {LoginContext} from "../contexts/LoginContext";
import {useHistory} from "react-router";
import {Link} from "react-router-dom";
import axios from "axios";
import {useSnackbar} from "notistack";
import {useStyles} from "../styles/styles";

/**
 * Component handling logging in users
 * Redirects the user home if they're already logged in
 */
const LoginForm = function () {
    let {enqueueSnackbar} = useSnackbar();
    const dispatch = useContext(LoginContext)[1];
    const history = useHistory();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);
    let historyRef = useRef(history);

    let classes = useStyles();

    useEffect(() => {
        axios.get("/api/ping").then((result) => {
            if (result.status === 200) {
                dispatch({ type: "login", username: result.data});
                historyRef.current.push("/");
            }
        });
    }, [historyRef, dispatch]);

    async function submitHandler(event) {
        event.preventDefault();
        setLoading(true);
        axios.post("/api/users/login", {username: username, password: password}).then(async (result) => {
            dispatch({
                type: "login",
                username: result.data.username
            });
            localStorage.setItem("sessionId", result.data.sessionId);
            historyRef.current.push("/");
        }).catch(() => {
            enqueueSnackbar("Incorrect username or password", {variant: "error"});
            setLoading(false);
        });
    }

    return <Card className={classes.loginCard}>
        <Backdrop open={loading} className={classes.backdrop}>
            <CircularProgress/>
        </Backdrop>
        <h1>Log in</h1>
        <form onSubmit={submitHandler}>
            <div>
                <TextField id="username" size="large" disabled={loading} label="User name" type="text" value={username}
                           onChange={(event) => setUsername(event.target.value)}/>
            </div>
            <div>
                <TextField id="password" size="large" disabled={loading} label="Password" type="password"
                           value={password} onChange={(event) => setPassword(event.target.value)}/>
            </div>
            <Button type="submit" variant="contained" color="primary" disabled={loading}
                    className={classes.loginButton}>Login</Button>
        </form>
        <p>Not registered yet? <Link to="/signup">Sign up</Link></p>
    </Card>;
};

export default LoginForm