import connectDB from "../../../middleware/mongodb";
import {useUser} from "../../../auth/hooks";
import EventModel from "../../../model/event";

const handler = async (req, res) => {
    if(req.method !== 'POST') {
        return res.status(405).send({message: "Method not allowed."});
    }
    let user = await useUser(req);
    if(!user) {
        return res.status(401).send("Not logged in");
    }
    let event = new EventModel({
        ...req.body,
        owner: user._id,
        participants: [user._id]
    });
    await event.save((error) => {
        if (error) {
            return res.status(400).send(error);
        }
        res.send(event);
    });
};

export default connectDB(handler);