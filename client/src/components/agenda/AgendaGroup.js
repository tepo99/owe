
const AgendaGroup = (props) => {
        
    let isNotAlone = props.events.length > 1;

    return(
        <div style={isNotAlone ? {borderLeft: "5px solid red"} : {}}>            
            {props.events}
        </div>
    );
};

export default AgendaGroup;