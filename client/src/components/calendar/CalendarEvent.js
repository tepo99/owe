import {Button, Container, Divider} from "@material-ui/core";
import {Link} from "react-router-dom";
import {useStyles} from "../../styles/styles";
import {format} from "date-fns";
import {People} from "@material-ui/icons";

/**
 * Component used for rendering a single event passed to it via props.event
 */
const CalendarEvent = function (props) {
    let classes = useStyles();

    if (props.isAgenda) {
        let startDate = new Date(props.event.startDate);
        let endDate = new Date(props.event.endDate);
        return (<><Container className={classes.agendaItem}>
            <Container><Link to={"/event/" + props.event._id}>{props.event.name}</Link></Container>
            <Container>
                <Container>From {format(startDate, "H:mm")}</Container>
                <Container>To: {startDate.getDate() === endDate.getDate() || format(endDate, "d. M. yyyy")} {format(endDate, "H:mm")}</Container>
            </Container>
            <Container>{props.event.place.length>1&&"At: " +props.event.place}</Container>
            <Container style={{display: "flex", gap: "0.5em", alignItems: "center"}}><People/> {props.event.participants.length}</Container>
        </Container>
            <Divider/>
        </>);
    }

    return <Container className={classes.eventWrapper}>
        <Link
            to={"/event/" + props.event._id}
        >
            <Button size="small" color="primary" variant="contained" disableElevation
                    className={classes.eventButton}>{props.event.name}</Button>
        </Link>
    </Container>;
};

export default CalendarEvent;