import connectDB from "../../../../middleware/mongodb";
import EventModel from "../../../../model/event";
import {useUser} from "../../../../auth/hooks";

const handler = async (req, res) => {
    if(req.method !== 'DELETE') {
        return res.status(405).send({message: "Method not allowed."});
    }
    let {_id} = req.query;
    let user = await useUser(req);
    if(!user) {
        return res.status(401).send("Not logged in");
    }
    EventModel.findOne({_id: _id}, (error, result) => {
        if (error) {
            return res.status(400).send(error);
        }
        if (!result) {
            return res.status(404).send();
        }
        if (result.owner === user._id) {
            EventModel.remove({_id: _id}).then(() => {
                return res.status(200).send();
            }).catch(error => {
                return res.status(500).send(error);
            });
        }
        if (result.participants.includes(user._id)) {
            result.participants = result.participants.filter((item) => item !== user._id);
            result.save();
            return res.status(200).send();
        }
    });
};

export default connectDB(handler);