import React, {useEffect, useState} from "react";
import {Backdrop, CircularProgress} from "@material-ui/core";
import TeamItem from "./TeamItem";
import axios from "axios";
import {useStyles} from "../../styles/styles";
import {useSnackbar} from "notistack";

/**
 * Component for fetching and displaying teams the user has created and manages
 */
const MyTeams = () => {
    let {enqueueSnackbar} = useSnackbar();
    let [teams, setTeams] = useState([]);
    let [loading, setLoading] = useState(true);
    let classes = useStyles();

    useEffect(() => {
        axios.get("/api/teams/my")
            .then((result) => {
                setTeams(result.data);
            }).catch((error) => {
            enqueueSnackbar("Failed to load teams" + (error.response.data.message&&": " + error.response.data.message), {variant: "error"});
        }).finally(() => setLoading(false));
    }, [enqueueSnackbar]);

    let getTeams = () => {
        return teams.map(team => <TeamItem key={team._id} team={team} editable/>);

    };

    return (<>
        <Backdrop open={loading} className={classes.backdrop}>
            <CircularProgress/>
        </Backdrop>
        <div>
            {getTeams()}
        </div>
    </>);
};

export default MyTeams;