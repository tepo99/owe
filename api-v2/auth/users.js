import UserModel from "../model/user";

export async function createUser({ username, password }) {
    try {
        return await UserModel.create({username: username, password: password});
    } catch (error) {
        return error;
    }
}

export async function findUser({ username }) {
    return await UserModel.findOne({username: username});
}

export function validatePassword(user, inputPassword) {
    return user.isValidPassword(inputPassword);
}