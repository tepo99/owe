import EventModel from "../../../../model/event";
import connectDB from "../../../../middleware/mongodb";
import {useUser} from "../../../../auth/hooks";

const handler = async (req, res) => {
    if(req.method !== 'POST') {
        return res.status(405).send({message: "Method not allowed."});
    }
    let {_id} = req.query;
    let user = await useUser(req);
    if (!user) {
        return res.status(401).send("Not logged in");
    }
    EventModel.find({_id: _id, owner: user._id}, async (error) => {
        if (error) {
            res.status(400).send(error);
        }
        let event = await EventModel.updateOne({_id: _id}, req.body);
        res.send(event);
    });
};

export default connectDB(handler);