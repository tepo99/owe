import EventModel from "../../../../model/event";
import connectDB from "../../../../middleware/mongodb";
import {useUser} from "../../../../auth/hooks";

const handler = async (req, res) => {
    if(req.method !== 'GET') {
        return res.status(405).send({message: "Method not allowed."});
    }
    let {params} = req.query;
    let user = await useUser(req);
    if (!user) {
        return res.status(401).send("Not logged in");
    }
    let year = params[0];
    let month = params[1];
    //vytvor ridici datumy
    let monthStart = new Date(year, month, 0, 0, 0, 0, 0);
    let monthEnd = new Date(year, 1 + (parseInt(month)), 1, 0, 0, 0, 0);
    //nacti udalosti dle rozsahu
    EventModel.find({
        $or: [{owner: user._id}, {participants: user._id}],
        startDate: {"$gt": monthStart, "$lt": monthEnd}
    }).sort({startDate: 1}).exec((error, result) => {
        if (error) {
            return res.status(400).send(error);
        }
        return res.status(200).send(result);
    });
};

export default connectDB(handler);