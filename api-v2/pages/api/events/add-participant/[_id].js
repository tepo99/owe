import connectDB from "../../../../middleware/mongodb";
import EventModel from "../../../../model/event";
import UserModel from "../../../../model/user";
import TeamModel from "../../../../model/team";
import {useUser} from "../../../../auth/hooks";

const handler = async (req, res) => {
    if(req.method !== 'POST') {
        return res.status(405).send({message: "Method not allowed."});
    }
    let {_id} = req.query;
    let event = await EventModel.findOne({_id: _id});
    let sessionUser = await useUser(req);
    if(!sessionUser) {
        return res.status(401).send("Not logged in");
    }
    if (!event) {
        return res.status(404).send();
    }
    let user = await UserModel.findOne({$or: [{username: req.body.search}, {email: req.body.search}]});
    let team = await TeamModel.findOne({name: req.body.search});
    if (user) { //it's a user! add him!
        EventModel.findOneAndUpdate({_id: event.id}, {$addToSet: {participants: user._id}}, {new: true}, async (error, event) => {
            if (error) {
                return res.status(400).send(error);
            }
            UserModel.find({_id: {$in: event.participants}}, "_id username email", (error, result) => {
                return res.status(200).send(result);
            });
        });
    }
    else if (team) { //it's a team! add them all!
        let members = team.members;
        let addedMembers = [];
        new Promise((resolve) => {
            members.forEach((member) => {
                EventModel.updateOne({_id: event.id}, {$addToSet: {participants: member}}).exec();
                addedMembers.push(member);
                if (members.length === addedMembers.length) {
                    resolve();
                }
            });
        }).then(async () => {
            event = await EventModel.findOne({_id: event._id});
            let newParticipants = await UserModel.find({_id: {$in: event.participants}}, "_id username email");
            return res.status(200).send(newParticipants);
        });
    } else {
        return res.status(404).send(req.body.search + " is neither a team or a user");
    }
};

export default connectDB(handler);