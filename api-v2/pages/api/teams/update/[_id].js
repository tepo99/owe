import TeamModel from "../../../../model/team";
import connectDB from "../../../../middleware/mongodb";
import {useUser} from "../../../../auth/hooks";

const handler = async (req, res) => {
    if(req.method !== 'POST') {
        return res.status(405).send({message: "Method not allowed."});
    }
    let {_id} = req.query;
    let user = await useUser(req);
    if (!user) {
        return res.status(401).send("Not logged in");
    }
    TeamModel.findOne({_id: _id}, (err, result) => {
        if (err) throw err;
        if (result.owner !== user._id) {
            return res.status(400).send("Cannot edit team you do not own.");
        }
        result.name = req.body.name;
        result.description = req.body.description;
        result.save();
        res.status(200).send(result);
    });
};

export default connectDB(handler);