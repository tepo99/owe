import {useContext, useEffect, useState} from 'react';
import {LoginContext} from "../contexts/LoginContext";
import {Redirect, Route} from "react-router-dom";
import axios from "axios";
import {CircularProgress} from "@material-ui/core";

/**
 * A route wrapper that redirects the user to /login if he is not logged in
 */
const ProtectedRoute = function (props) {
    const [context, dispatch] = useContext(LoginContext);
    let [loggedIn, setLoggedIn] = useState(context.loggedIn);
    let [loading, setLoading] = useState(true);

    useEffect(() => {
        axios.get("/api/ping").then((result) => {
            if (result.status === 200 || result.status === 304) {
                dispatch({ type: "login", username: result.data});
                setLoggedIn(true);
            }
        }).catch(() => {
            setLoading(false);
        }).finally(() => setLoading(false));
    }, [dispatch]);

    if(loading) {
        return <CircularProgress/>;
    }

    if (loggedIn) {
        return <Route path={props.path} component={props.component}/>;
    }

    return <Redirect to="/login"/>;
};

export default ProtectedRoute;