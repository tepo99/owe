import UserModel from "../../../model/user";
import {useUser} from "../../../auth/hooks";
import connectDB from "../../../middleware/mongodb";

const handler = (req, res) => {
    if(req.method !== 'POST') {
        return res.status(405).send({message: "Method not allowed."});
    }
    let user = useUser(req);
    if(!user) {
        return res.status(401).send();
    }
    UserModel.find({_id: {$in: req.body}}, "_id username email", (error, result) => {
        if (error) {
            return res.status(400).send(error);
        }
        res.status(200).send(result);
    });
};

export default connectDB(handler);