import {IconButton, InputLabel, TextField} from "@material-ui/core";
import {useState} from "react";
import {useSnackbar} from "notistack";
import {Add} from "@material-ui/icons";
import {useStyles} from "../../styles/styles";

/**
 * Renders and validates the team form. Must be provided a team in the props as well as a handler to edit it
 */
const TeamForm = (props) => {
    let {enqueueSnackbar} = useSnackbar();
    let [nameError, setNameError] = useState("");
    let classes = useStyles();

    let handleNameChange = (event) => {
        props.editHandler({name: event.target.value});
        if (event.target.value === "") {
            return setNameError("Field required");
        }
        return setNameError("");
    };

    let handleDescriptionChange = (event) => {
        props.editHandler({description: event.target.value});
    };

    let handleSubmit = (event) => {
        event.preventDefault();
        if (nameError) {
            return enqueueSnackbar("Event was not saved. Please fix the highlighted fields.");
        }
        props.submitHandler();
    };

    return (<form onSubmit={handleSubmit} className={classes.form}>
        <IconButton type="submit" className={classes.addButton}><Add/></IconButton>
        <InputLabel htmlFor="team-name">Name</InputLabel>
        <TextField id="team-name" type="text" required error={nameError} disabled={props.loading} helperText={nameError}
                   value={props.team.name} onChange={handleNameChange}/>
        <InputLabel htmlFor="team-description">Description</InputLabel>
        <TextField id="team-description" type="textarea" multiline rows={5} disabled={props.loading}
                   value={props.team.description} onChange={handleDescriptionChange}/>
    </form>);
};

export default TeamForm;