import {useEffect, useState} from "react";
import UserList from "../users/UserList";
import {Button, CircularProgress, Paper, TextField} from "@material-ui/core";
import axios from "axios";
import {useSnackbar} from "notistack";
import {Add} from "@material-ui/icons";
import {useStyles} from "../../styles/styles";

/**
 * Component handling displaying and adding members of a team
 */
const MemberPicker = (props) => {
    let {enqueueSnackbar} = useSnackbar();
    let [members, setMembers] = useState([]);
    let [loading, setLoading] = useState(true);
    let [newMember, setNewMember] = useState("");
    let classes = useStyles();

    useEffect(() => {
        if (props.members.length > 0) {
            axios.post("/api/users/info", props.members).then((result) => {
                setMembers(result.data);
            }).catch((error) => {
                enqueueSnackbar("Failed to load members" + error.message&&": " + error.message, {variant: "error"});
            }).finally(() => setLoading(false));
        } else {
            setLoading(false);
        }
    }, [props.members, enqueueSnackbar]);

    let handleMemberAdd = () => {
        axios.post("/api/teams/add-member/" + props.teamId, {search: newMember})
            .then((result) => {
                setMembers([...members, result.data]);
                enqueueSnackbar("Team member added", {variant: "success"});
                setNewMember("");
            }).catch((error) => {
            enqueueSnackbar("Failed to add member" + (error.response.data.message&&": " + error.response.data.message), {variant: "error"});
        });
    };

    let handleMemberDelete = (id) => {
        setLoading(true);
        axios.post("/api/teams/remove-member/" + props.teamId, {_id: id}).then(() => {
            setMembers(members.filter(member => member._id !== id));
            enqueueSnackbar("Member removed", {variant: "success"});
            setLoading(false);
        }).catch((error) => {
            enqueueSnackbar("Failed to remove member" + (error.response.data.message&&": " + error.response.data.message), {variant: "error"});
        });
    };

    if (loading) {
        return <CircularProgress/>;
    }

    return (<>
        <Paper className={classes.wrapper}>
            <UserList users={members} onParticipantDelete={handleMemberDelete}/>
            <Paper className={classes.formWrapper}>
                    <TextField type="text" placeholder="Add member" value={newMember} onChange={(event) => {
                        setNewMember(event.target.value);
                    }}/>
                    <Button variant="contained" color="primary" disableElevation size="small" onClick={() => handleMemberAdd()}><Add/></Button>
            </Paper>
        </Paper>
    </>);
};

export default MemberPicker;