import {Button, Paper} from "@material-ui/core";
import {Link, useHistory} from "react-router-dom";
import axios from "axios";
import {useSnackbar} from "notistack";

const AgendaItem = function (props) {
    let {enqueueSnackbar} = useSnackbar();

    const deleteItem = () => {
        axios.delete("/api/events/delete/" + props.event._id)
        .then(() => history.go(0) ).catch((error) => {
            enqueueSnackbar("Failed to delete event" + (error.response.data.message&&": " + error.response.data.message), {variant: "error"});
        });
    };

    let history = useHistory();

    return (
        <Paper style={{display: "flex", alignItems: "center"}}>
            <Link to={"/event/" + props.event._id} style={{overflow: "hidden", whiteSpace: "nowrap"}}>{props.event.name}</Link>
            <p style={{paddingLeft: "10px"}}>Place: {props.event.place}</p>
            <Button style={{float:"right"}} onClick={deleteItem}>D</Button>
        </Paper>
    );
};

export default AgendaItem;