import React, {useContext, useEffect, useState} from "react";
import {Backdrop, Card, CircularProgress, IconButton, InputLabel, TextField} from "@material-ui/core";
import {useHistory, useParams} from "react-router";
import {DateTimePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import {LoginContext} from "../../contexts/LoginContext";
import ParticipantsPicker from "./ParticipantsPicker";
import axios from "axios";
import {useSnackbar} from "notistack";
import Navigation from "../Navigation";
import {Add, Delete, SaveAlt} from "@material-ui/icons";
import {useStyles} from "../../styles/styles";

/**
 * Event form component responsible for creating and updating events on the api
 */
const EventForm = function () {
    let history = useHistory();
    let {enqueueSnackbar} = useSnackbar();
    let newDateStart = new Date();
    let newDateEnd = new Date(newDateStart).setMinutes(30 + newDateStart.getMinutes());
    let {id, date} = useParams();
    let [context] = useContext(LoginContext);
    let [loading, setLoading] = useState(true);
    let [name, setName] = useState("");
    let [nameError, setNameError] = useState("");
    let [place, setPlace] = useState("");
    let [startDate, setStartDate] = useState(newDateStart);
    let [startDateError, setStartDateError] = useState("");
    let [endDate, setEndDate] = useState(newDateEnd);
    let [endDateError, setEndDateError] = useState("");
    let [description, setDescription] = useState("");
    let [participants, setParticipants] = useState([]);
    let classes = useStyles();

    useEffect(() => {
        if (!id) { //creating a new event, initialize dates
            if (date) {
                let newDateStart = new Date(date);
                let newDateEnd = new Date(date).setMinutes(30 + newDateStart.getMinutes());
                setStartDate(newDateStart);
                setEndDate(newDateEnd);
            }
            setLoading(false);
            return;
        }
        //fetching existing events
        axios.get("/api/events/get/" + id).then(
            result => {
                let json = result.data;
                setName(json.name);
                setPlace(json.place);
                setStartDate(new Date(json.startDate));
                setEndDate(new Date(json.endDate));
                setDescription(json.description);
                setParticipants(json.participants);
                setLoading(false);
            }).catch((error) => {
                enqueueSnackbar("Failed to load event" + (error.response.data.message&&": " + error.response.data.message), {variant: "error"});
                setLoading(false);
            });
    }, [id, date, context.token, enqueueSnackbar]);


    let handleSubmit = async (e) => {
        e.preventDefault();
        if (nameError || startDateError || endDateError) {
            return enqueueSnackbar("Event was not saved. Please fix the highlighted fields.", {variant: "error"});
        }
        setLoading(true);
        let event = {
            name: name,
            place: place,
            startDate: startDate,
            endDate: endDate,
            description: description
        };
        if (id) {
            event["_id"] = id;
            axios.post("/api/events/update/" + id, event).then(() => {
                enqueueSnackbar(`Event ${event.name} saved`, {variant: "success"});
            }).catch((error) => {
                enqueueSnackbar("Failed to update event" + (error.response.data.message&&": " + error.response.data.message), {variant: "error"});
            }).finally(() => setLoading(false));
        } else {
            axios.post("/api/events/add", event).then(() => {
                enqueueSnackbar(`Event ${event.name} added`, {variant: "success"});
            }).catch((error) => {
                enqueueSnackbar("Failed to add event" + (error.response.data.message&&": " + error.response.data.message), {variant: "error"});
            }).finally(() => setLoading(false));
        }
    };

    let handleNameChange = (e) => {
        setName(e.target.value);
        if (e.target.value === "") {
            return setNameError("Field required");
        }
        return setNameError("");
    };

    let handleStartDateChange = (date) => {
        setStartDate(date);
        if (!date) {
            return setStartDateError("Field required");
        }
        return setStartDateError("");
    };

    let handleEndDateChange = (date) => {
        setEndDate(date);
        if (!date) {
            return setEndDateError("Field required");
        }
        if (date < startDate) {
            return setEndDateError("Event can't end before it starts");
        }
        return setEndDateError("");
    };

    let handleDelete = () => {
        setLoading(true);
        axios.delete("/events/delete/"+id).then((response) =>{
            if(response.status === 200) {
                enqueueSnackbar("Event deleted", {variant: "success"});
                return history.push("/");
            }
        }).catch((error) => {
            enqueueSnackbar("Failed to delete event" + (error.response.data.message&&": " + error.response.data.message), {variant: "error"});
            setLoading(false);
        });
    };

    return <Card className={classes.column}>
        <Backdrop open={loading} className={classes.backdrop}>
            <CircularProgress/>
        </Backdrop>
        <form onSubmit={handleSubmit} className={classes.formOuter}>
            <Navigation>
                <div className={classes.autoCenter}/>
                <IconButton onClick={handleDelete}><Delete/></IconButton>
            </Navigation>
            <div className={classes.form}>
                <InputLabel htmlFor="event-name">Name:</InputLabel>
                <TextField id="event-name" disabled={loading} required error={!!nameError} helperText={nameError}
                           onChange={handleNameChange} value={name}/>
                <InputLabel htmlFor="event-place">Place:</InputLabel>
                <TextField id="event-place" disabled={loading} onChange={(e) => {
                    setPlace(e.target.value);
                }} value={place}/>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <InputLabel htmlFor="event-start-date">From:</InputLabel>
                    <DateTimePicker id="event-start-date" disabled={loading} required error={!!startDateError}
                                    helperText={startDateError} onChange={handleStartDateChange} value={startDate}/>
                    <InputLabel htmlFor="event-end-date">To:</InputLabel>
                    <DateTimePicker id="event-end-date" disabled={loading} required error={!!endDateError}
                                    helperText={endDateError} onChange={handleEndDateChange} value={endDate}/>
                </MuiPickersUtilsProvider>
                <InputLabel htmlFor="event-description">Description:</InputLabel>
                <TextField id="event-description" multiline rows={5} disabled={loading} onChange={(e) => {
                    setDescription(e.target.value);
                }} value={description}/>
                {id && !loading && <ParticipantsPicker eventId={id} participants={participants}/>}
            </div>
            <IconButton type="submit" variant="contained" disabled={loading}
                        className={classes.addButton}>{id ? <SaveAlt/> : <Add/>}</IconButton>
        </form>
    </Card>;
};

export default EventForm;