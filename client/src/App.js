import './bootstrap';
import './App.css';
import React from "react";
import {LoginProvider} from './contexts/LoginContext';
import Main from "./components/Main";
import {BrowserRouter} from "react-router-dom";
import {SnackbarProvider} from "notistack";
import {ThemeProvider} from "@material-ui/core/styles"
import {brightTheme} from "./styles/theme";

function App() {
    return (
        <div className="App">
            <ThemeProvider theme={brightTheme}>
                <LoginProvider>
                    <SnackbarProvider>
                        <BrowserRouter>
                            <Main/>
                        </BrowserRouter>
                    </SnackbarProvider>
                </LoginProvider>
            </ThemeProvider>
        </div>
    );
}

export default App;
