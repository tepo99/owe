import connectDB from "../../../../middleware/mongodb";
import TeamModel from "../../../../model/team";
import {useUser} from "../../../../auth/hooks";

const handler = async (req, res) => {
    if(req.method !== 'DELETE') {
        return res.status(405).send({message: "Method not allowed."});
    }
    let {_id} = req.query;
    let user = await useUser(req);
    if (!user) {
        return res.status(401).send("Not logged in");
    }
    TeamModel.findOne({_id: _id}, (err, result) => {
        if (err) throw err;
        if (!result) {
            return res.status(404).send("Team not found");
        }
        if (result.owner !== user._id) {//user's not the owner
            if (result.members.includes(user._id)) {//but hey, they're a member
                result.members = result.members.filter(member => member !== user.id); //remove user from members
                result.save();
                return res.status(200).send();
            }
            //uh oh
            return res.status(400).send("Cannot delete a team you do not own");
        }
        //user is the owner
        TeamModel.findOneAndDelete({_id: _id}, (err) => {
            if (err) {
                return res.status(500).send(err);
            }
            res.status(200).send();
        });
    });
};

export default connectDB(handler);