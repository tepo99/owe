import TeamModel from "../../../../model/team";
import connectDB from "../../../../middleware/mongodb";
import {useUser} from "../../../../auth/hooks";

const handler = async (req, res) => {
    if(req.method !== 'GET') {
        return res.status(405).send({message: "Method not allowed."});
    }
    let {_id} = req.query;
    let user = await useUser(req);
    if (!user) {
        return res.status(401).send("Not logged in");
    }
    TeamModel.findOne({_id: _id}, (error, result) => {
        if (error) {
            return res.status(400).send(error);
        }
        if (!result) {
            return res.status(404).send();
        }
        res.status(200).send(result);
    });
};

export default connectDB(handler);