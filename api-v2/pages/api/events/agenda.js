import connectDB from "../../../middleware/mongodb";
import EventModel from "../../../model/event";
import {useUser} from "../../../auth/hooks";

const handler = async (req, res) => {
    if(req.method !== 'GET') {
        return res.status(405).send({message: "Method not allowed."});
    }
    let user = await useUser(req);
    if(!user) {
        return res.send("User not found");
    }
    let currentDate = new Date();
    let events = await EventModel.find({
        owner: user._id,
        startDate: {"$gt": currentDate}
    }).limit(50).sort({startDate: 1}).exec();
    res.send(events);

};

export default connectDB(handler);