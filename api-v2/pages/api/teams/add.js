import TeamModel from "../../../model/team";
import connectDB from "../../../middleware/mongodb";
import {useUser} from "../../../auth/hooks";

const handler = async (req, res) => {
    if(req.method !== 'POST') {
        return res.status(405).send({message: "Method not allowed."});
    }
    let user = await useUser(req);
    if (!user) {
        return res.status(401).send("Not logged in");
    }
    let team = new TeamModel({
        owner: user._id,
        name: req.body.name,
        description: req.body.description
    });
    team.save((error) => {
        if (error) {
            return res.status(400).send(error);
        }
        return res.status(200).send(team);
    });
};

export default connectDB(handler);