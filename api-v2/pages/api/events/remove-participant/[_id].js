import EventModel from "../../../../model/event";
import {useUser} from "../../../../auth/hooks";
import connectDB from "../../../../middleware/mongodb";

const handler = async (req, res) => {
    if(req.method !== 'POST') {
        return res.status(405).send({message: "Method not allowed."});
    }
    let {_id} = req.query;
    let event = await EventModel.findOne({_id: _id});
    let sessionUser = await useUser(req);
    if(!sessionUser) {
        return res.status(401).send("Not logged in");
    }
    if (!event) {
        return res.status(404).send();
    }
    // noinspection EqualityComparisonWithCoercionJS (disabled in favor of forced typecasting)
    event.participants = event.participants.filter(participant => participant != req.body._id);
    event.save();
    return res.status(200).send();
};

export default connectDB(handler);