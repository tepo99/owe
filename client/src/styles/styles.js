import {makeStyles} from "@material-ui/styles";

export const useStyles = makeStyles((theme) => ({
    root: {
        minHeight: "100vh",
        width: "100%",
        maxWidth: "1200px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        padding: "0",
        margin: "0 auto"
    },
    backdrop: {
        zIndex: 1000,
    },
    column: {
        display: "flex",
        flexDirection: "column",
        margin: "0",
        padding: "0",
        minHeight: "100vh",
        width: "100%",
    },
    row: {
        display: "flex",
        flexDirection: "row",
        margin: "0",
        padding: "0"
    },
    autoCenter: {
        margin: "auto",
    },
    appBar: {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        marginLeft: "auto",
        marginRight: "auto",
        "& *": {
            color: "inherit"
        }
    },
    //login styles
    loginCard: {
        margin: "auto",
        padding: "1em",
        maxWidth: "500px",
        maxHeight: "500px",
    },
    loginButton: {
        margin: "1em",
    },
    //calendar styles
    calendarRoot: {
        display: "flex",
        flexDirection: "column",
        width: "100vw",
        maxWidth: "100%",
        height: "100vh"
    },
    calendarHeader: {
        flexGrow: 0,
        padding: "0",
        margin: "0"
    },
    monthPicker: {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        marginLeft: "auto",
        marginRight: "auto"
    },
    //month styles
    month: {
        flexGrow: 1,
        padding: "0",
        margin: "0",
        display: "flex",
        flexDirection: "column",
    },
    weekContainer: {
        padding: "0",
        flexGrow: 1,
    },
    //week styles
    weekGrid: {
        height: "100%",
        display: "grid",
        gridTemplateColumns: "repeat(7, calc(100%/7))"
    },
    dayWrapper: {
        width: "100%",
        display: "flex",
        flexDirection: "row",
        margin: "0",
        padding: "0"
    },
    //dayStyles
    day: {
        width: "100%",
        height: "100%",
        padding: "0",
        margin: "0"
    },
    today: {
        padding: "0",
        margin: "0",
        backgroundColor: theme.palette.primary["50"],
        outline: "1px solid",
        outlineColor: theme.palette.primary["700"],
        zIndex: 500,
    },
    foreignDay: {
        padding: "0",
        margin: "0",
        backgroundColor: "#eeeeee"
    },
    eventsWrapper: {
        display: "flex",
        flexDirection: "column",
        margin: "0",
        padding: "0",
        height: "100%",
        '& a': {
            textDecoration: "none",
        }
    },
    addEventButton: {
        margin: "auto .5em .5em .5em",
    },
    singleEvent: {width: "100%", margin: "0", padding: "0"},
    eventWrapper: {
        '& a': {
            width: "100%"
        },
        display: "flex",
        alignItems: "center",
        margin: "0",
        padding: "0",
        width: "100%"
    },
    eventButton: {
        "& .MuiButton-label": {
            fontSize: "0.75em",
            fontWeight: "normal"
        },
        display: "flex",
        border: "none",
        alignItems: "self-start",
        padding: "0 0.5em",
        width: "100%",
        overflow: "hidden",
        whiteSpace: "nowrap",
        marginLeft: "auto",
        marginRight: "auto",
        justifyContent: "left",
    },
    //form styles
    formOuter: {
        position: "relative",
        height: "100%"
    },
    form: {
        display: "flex",
        flexDirection: "column",
        height: "100%",
        padding: "1em",
    },
    addButton: {
        backgroundColor: theme.palette.primary.A700,
        position: "absolute",
        bottom: "1em",
        right: "1em"
    },
    //participants
    participantsWrapper: {
        display: "flex",
        flexDirection: "column",
        margin: "0",
        padding: "0",
    },
    participantsFormWrapper: {
        width: "100%",
        padding: "0.5em"
    },
    //team item
    outer: {
        margin: "1em"
    },
    grid: {
        display: "grid",
        gridTemplateColumns: "10% 80% 10%",
        padding: "0.5em 0"
    },
    edit: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-start",
        padding: "0",
        margin: "0",
        alignItems: "center"
    },
    members: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-end",
        alignItems: "center"
    },
    //user list
    wrapper: {
        width: "100%",
        display: "flex",
        alignItems: "center",
        padding: "0",
        flexWrap: "wrap",
        justifyContent: "space-evenly",
    },
    item: {
        margin: "0.5em"
    },
    agendaDay: {
        backgroundColor: theme.palette.primary[50],
        padding: "0.5em",

    },
    agendaItem: {
        margin: "0.5em",
        display: "grid",
        gridTemplateColumns: "25% 40% 25% 10%",
        alignItems: "center"
    }
}), {index: 1});