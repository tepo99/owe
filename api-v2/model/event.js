import mongoose from 'mongoose'
import {Schema} from 'mongoose'
import UserModel from "./user";

/**
 * Defines a Mongoose schema for a calendar event entry
 * @type {Schema}
 */
const EventSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        ref: UserModel,
        required: true
    },
    place: String,
    startDate: {
        type: Date,
        required: true
    },
    endDate: {
        type: Date,
        required: true
    },
    description: String,
    participants:[{type: mongoose.Schema.Types.ObjectId, ref: UserModel}],
});

EventSchema.path("_id");

const EventModel = mongoose.models.Event || mongoose.model("Event", EventSchema);

export default EventModel;