import {getLoginSession} from "./auth";
import {findUser} from "./users";

export async function useUser(req) {
    const session = await getLoginSession(req);
    return (session && (await findUser(session))) ?? null;
}