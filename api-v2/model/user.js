import mongoose, {Schema} from 'mongoose';
import bcrypt from 'bcryptjs';

/**
 * Defines a Mongoose schema for a singular user of the calendar app
 * @type {Schema}
 */
const UserSchema = new Schema({
    username: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    }
});

UserSchema.pre(
    'save',
    async function(next) {
        const user = this;
        this.password = await bcrypt.hash(user.password, 10);
        next();
    }
);

UserSchema.methods.isValidPassword = async function(password) {
    const user = this;
    return bcrypt.compare(password, user.password);
};

const UserModel = mongoose.models.User || mongoose.model("User", UserSchema);

export default UserModel;

