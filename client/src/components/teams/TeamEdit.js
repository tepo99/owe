import React, {useEffect, useState} from "react";
import {useHistory, useParams} from "react-router";
import Navigation from "../Navigation";
import TeamForm from "./TeamForm";
import axios from "axios";
import {useSnackbar} from "notistack";
import {Backdrop, Card, CircularProgress, IconButton} from "@material-ui/core";
import {useStyles} from "../../styles/styles";
import {Delete} from "@material-ui/icons";

/**
 * Handler component for editing an existing team via the TeamForm component
 */
const TeamEdit = () => {
    let {enqueueSnackbar} = useSnackbar();
    let history = useHistory();
    let params = useParams();
    let [loading, setLoading] = useState(true);
    let [team, setTeam] = useState({});
    let classes = useStyles();

    useEffect(() => {
        axios.get("/api/teams/get/" + params.id)
            .then((result) => {
                setTeam(result.data);
            }).catch((error) => {
            enqueueSnackbar("Failed to load team" + (error.response.data.message&&": " + error.response.data.message), {variant: "error"});
        }).finally(() => setLoading(false));
    }, [params.id, enqueueSnackbar]);

    let handleTeamEdit = (edit) => {
        setTeam({...team, ...edit});
    };
    let handleTeamSubmit = () => {
        setLoading(true);
        axios.post("/api/teams/update/" + params.id)
            .then(() => {
                enqueueSnackbar(`Team ${team.name} saved successfully`, {variant: "success"});
            }).catch((error) => {
            enqueueSnackbar("Failed to save team" + (error.response.data.message&&": " + error.response.data.message), {variant: "error"});
        }).finally(() =>setLoading(false));
    };

    let handleDelete = () => {
        setLoading(true);
        axios.delete("/api/teams/delete/"+params.id).then((response) => {
            if(response.status === 200) {
                enqueueSnackbar("Team deleted", {variant: "success"});
                history.goBack();
            }
        }).catch((error) => {
            enqueueSnackbar("Failed to delete team" + (error.response.data.message&&": " + error.response.data.message), {variant: "error"});
            setLoading(false);
        });
    };

    return (<Card className={classes.column}>
        <Navigation>
            <div className={classes.autoCenter}/>
            <IconButton onClick={handleDelete}><Delete/></IconButton>
        </Navigation>
        <Backdrop open={loading} className={classes.backdrop}>
            <CircularProgress/>
        </Backdrop>
        <TeamForm team={team} loading={loading} editHandler={handleTeamEdit} submitHandler={handleTeamSubmit}/>
    </Card>);

};

export default TeamEdit;