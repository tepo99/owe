import React, {useEffect, useState} from 'react';
import Week from "./Week";
import {Backdrop, CircularProgress, Container, Divider} from "@material-ui/core";
import axios from "axios";
import {useStyles} from "../../styles/styles";
import {useSnackbar} from "notistack";

/**
 *  Component responsible for fetching and rendering a month of the calendar
 */
const Month = function (props) {
    let {enqueueSnackbar} = useSnackbar();
    let [loading, setLoading] = useState(true);
    let [events, setEvents] = useState([]);
    let classes = useStyles();

    useEffect(() => {
        axios.get(`/api/events/list/${props.year}/${props.month}`)
            .then(result => {
                let data = result.data;
                setEvents(data);
            }).catch((error) => {
            enqueueSnackbar("Failed to load events " + (error.response.data.message&&": " + error.response.data.message), {variant: "error"});
        }).finally(() => setLoading(false));
    }, [props.year, props.month, enqueueSnackbar]);

    let startOfMonth = new Date(props.year, props.month, 1);
    let endOfMonth = new Date(props.year, props.month + 1, 0);
    let daysInMonth = startOfMonth.getDay() + endOfMonth.getDate() - 1;
    let weeksInMonth = Math.ceil(daysInMonth / 7);

    const getWeeks = () => {
        let children = [];
        for (let i = 0; i < weeksInMonth; i++) {
            children.push(<Container className={classes.weekContainer} key={`${props.year}-${props.month}-week${i}`}>
                    <Week
                        year={props.year}
                        month={props.month}
                        week={i}
                        events={events}
                        weeks={weeksInMonth}
                    />
                    {i !== weeksInMonth - 1 && <Divider/>}
                </Container>
            );
        }
        return children;
    };

    return <>
        <Backdrop open={loading} className={classes.backdrop}>
            <CircularProgress/>
        </Backdrop>
        <Container className={classes.month} key={`wrapper${props.year}-${props.month}`}>
            {getWeeks()}
        </Container>
    </>;
};

export default Month;