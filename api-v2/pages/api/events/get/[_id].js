import connectDB from "../../../../middleware/mongodb";
import EventModel from "../../../../model/event";
import {useUser} from "../../../../auth/hooks";

const handler = async (req, res) => {
    if(req.method !== 'GET') {
        return res.status(405).send({message: "Method not allowed."});
    }
    let {_id} = req.query;
    let user = await useUser(req);
    if(!user) {
        return res.status(401).send("Not logged in");
    }
    EventModel.findOne({_id: _id, $or: [{owner: user._id}, {participants: user._id}]}, (error, result) => {
        if (error) {
            return res.status(400).send(error);
        }
        if (null === result) {
            return res.status(404).send();
        }
        return res.send(result);
    });
};

export default connectDB(handler);
