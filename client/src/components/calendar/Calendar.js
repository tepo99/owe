import React from 'react';
import {Card, Container, IconButton} from "@material-ui/core";
import Month from "./Month";
import {useHistory, useParams} from "react-router";
import Navigation from "../Navigation";
import {DatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import {Link} from "react-router-dom";
import {Add, ArrowLeft, ArrowRight} from "@material-ui/icons";
import {useStyles} from "../../styles/styles";

/**
 * Component handling month switching in the calendar
 */
const Calendar = () => {
    let history = useHistory();
    let {year, month} = useParams();
    let classes = useStyles();

    let date = new Date(year, month);

    const handleMonthChange = (event) => {
        history.push(`/${event.getFullYear()}/${event.getMonth()}`);
    };

    return <Card className={classes.calendarRoot}>
        <Container className={classes.calendarHeader}>
            <Navigation>
                <Container className={classes.monthPicker}>
                    <IconButton onClick={() => {
                        history.push(previousMonth(year, month));
                    }}><ArrowLeft/></IconButton>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <DatePicker value={date} format="MMMM yyyy" onChange={handleMonthChange}/>
                    </MuiPickersUtilsProvider>
                    <IconButton onClick={() => {
                        history.push(nextMonth(year, month));
                    }}><ArrowRight/></IconButton>
                </Container>
                <Link to="/event/add"><IconButton><Add/></IconButton></Link>
            </Navigation>
        </Container>
        <Month year={year} month={month}/>
    </Card>;
};

const previousMonth = function (year, month) {
    let newMonth = parseInt(month) - 1;
    if (newMonth < 0) {
        return `/${parseInt(year) - 1}/11`;
    } else {
        return `/${year}/${newMonth}`;
    }
};

const nextMonth = function (year, month) {
    let newMonth = parseInt(month) + 1;
    if (newMonth > 11) {
        return `/${parseInt(year) + 1}/0`;
    } else {
        return `/${year}/${newMonth}`;
    }
};

export default Calendar